Česká republika měla od svého vzniku téměř před pětadvaceti lety 13 vlád, jen dvě z nich vydržely po celé funkční období: první vláda Václava Klause (1992-1996) a vláda Miloše Zemana (1998-2002).

Kabinet Bohuslava Sobotky, který do konce týdne podá demisi, bude s 1192 dny u moci (pokud ohlášenou demisi stihne do pátku) třetím nejdéle sloužícím kabinetem v historii.

Naopak nejkratší trvání (127 dnů) měla na přelomu let 2006 a 2007 prv ní vláda Mirka Topolánka. Jen 201, respektive 203 dnů vydržely ve Strakově akademii úřednické kabinety Josefa Tošovského (1998) a Jiřáho Rusnoka (2013-2014).

Najetím myší nad sloupce v grafu zobrazíte podrobnosti: od kdy do kdy kabinet vládnul, jaké bylo jeho složení a důvod demise.  